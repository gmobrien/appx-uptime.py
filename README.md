***appx-uptime.py*** *has been ported to Go, future development will happen in the [appxuptime](https://gitlab.com/gmobrien/appxuptime) project.*

# appx-uptime.py

This is a simple script I use to generate a cute uptime message in plain
English.

It probably has no practical value.

## Usage

`./appx-uptime.py`

This will produce output on STDOUT similar to:

`This server has been up for about 2 days.`

Enjoy!
